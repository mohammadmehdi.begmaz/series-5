package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {

        for (int i = 0; i < size - 1; i++) {
            int min = i ;
            for (int j = i + 1; j < size; j++) {
                if (arr[j] < arr[min]) {
                    min = j ;
                }
            }
            int temp = arr[min];
            arr[min] = arr[i];
            arr[i] = temp;
        }

        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for (int i = 1; i < size ; i++) {
            int j = i - 1;
            int temp = arr[i];

            while (j >= 0 && arr[j] > temp) {
                arr[j + 1] = arr[j];
                j-- ;
            }
            arr[j + 1] = temp;
        }

        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     */
    public int[] mergeSort(int[] arr, int size) {
        mergeSort2(arr,size);
        return arr;
    }

    public static void mergeSort2(int[] arr, int size) {
        if (size < 2) {
            return;
        }
        int midindex = size / 2;
        int[] lowindex = new int[midindex];
        int[] highindex = new int[size - midindex];

        for (int i = 0; i < midindex; i++) {
            lowindex[i] = arr[i];
        }
        for (int i = midindex; i < size; i++) {
            highindex[i - midindex] = arr[i];
        }
        mergeSort2(lowindex, midindex);
        mergeSort2(highindex, size - midindex);
        merge(arr, lowindex, highindex, midindex, size - midindex);
    }
    public static void merge(int[] arr, int[] lowindex, int[] highindex, int left, int right) {

        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (lowindex[i] <= highindex[j]) {
                arr[k++] = lowindex[i++];
            }
            else {
                arr[k++] = highindex[j++];
            }
        }
        while (i < left) {
            arr[k++] = lowindex[i++];
        }
        while (j < right) {
            arr[k++] = highindex[j++];
        }
    }


    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int index = -1;
        int lowindex = 0 ;
        int highindex = arr.length - 1 ;

        while (lowindex <= highindex ) {
            int midindex = (lowindex + highindex) / 2;
            if (arr[midindex] < value) {
                lowindex = midindex + 1;
            }
            if (arr[midindex] > value) {
                highindex = midindex - 1;
            }
            if (arr[midindex] == value) {
                index = midindex ;
                break;
            }
        }
        return index;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */

    public int binarySearchRecursive(int[] arr, int value) {
        return binarySearchr(arr,0,arr.length-1,value);
    }
    public int binarySearchr(int arr[], int lowindex , int highindex , int value) {

        if (highindex >= lowindex && lowindex < arr.length-1 ) {

            int midindex = lowindex + (highindex - lowindex) / 2;

            if (arr[midindex] == value) {
                return midindex;
            }


            if (arr[midindex] > value) {
                return binarySearchr(arr, lowindex, midindex - 1, value);
            }

            return binarySearchr(arr, midindex + 1, highindex, value);
        }

        return -1;
    }
}
