package sbu.cs;

public class pinkindex implements whitefunction{



    @Override
    public String func1(String arg1, String arg2) {
        String finaly = "";
        int j = 0 ;
        for(int i = 0 ; i < arg1.length() ; i++){
            if(j <= arg2.length()){
                finaly = finaly + arg1.charAt(i) + arg2.charAt(j) ;
            }
            else{
                finaly = finaly + arg1.charAt(i) ;
            }
            j++ ;
        }
        while(j < arg2.length()){
            finaly = finaly + arg2.charAt(j) ;
            j++ ;
        }
        return finaly ;
    }

    @Override
    public String func2(String arg1, String arg2) {
        String finaly = arg1 + new StringBuilder(arg2).reverse().toString();
        return finaly ;
    }

    @Override
    public String func3(String arg1, String arg2) {
        String finaly = "" ;
        int j = arg2.length() - 1 ;
        for(int i = 0 ; i < arg1.length() ; i++){
            if(j >= 0){
                finaly = finaly + arg1.charAt(i) + arg2.charAt(j) ;
            }
            else{
                finaly = finaly + arg1.charAt(i) ;
            }
            j-- ;
        }
        while(j >= 0){
            finaly = finaly + arg2.charAt(j);
            j-- ;
        }

        return finaly;
    }

    @Override
    public String func4(String arg1, String arg2) {
        if(arg1.length()%2 == 0){
            return arg1 ;
        }
        return arg2 ;
    }

    @Override
    public String func5(String arg1, String arg2) {
        StringBuilder sb = new StringBuilder();
        if (arg1.length() == arg2.length())
        {
            for (int i = 0; i < arg1.length(); i++)
            {
                sb.append(cm(arg1.charAt(i),arg2.charAt(i)));
            }
            return sb.toString();
        }
        else if (arg1.length() > arg2.length())
        {
            for (int i = 0; i < arg2.length(); i++)
            {
                sb.append(cm(arg1.charAt(i),arg2.charAt(i)));
            }
            sb.append(arg1.substring(arg2.length()));
            return sb.toString();
        }
        else
        {
            for (int i = 0; i < arg1.length(); i++)
            {
                sb.append(cm(arg1.charAt(i),arg2.charAt(i)));
            }
            sb.append(arg2.substring(arg1.length()));
            return sb.toString();
        }
    }
    private String cm(char i,char j)
    {
        char c =(char)((i + j - 2*'a') % 26 + 'a');
        String result;
        result = "" + c;
        return result;
    }
    }

