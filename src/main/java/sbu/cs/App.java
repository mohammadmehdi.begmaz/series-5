package sbu.cs;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {

        String finaly[][] = new String[n][n];

        firstindex test = new firstindex();
        finaly[0][0] = test.firstindex(arr, input);

        greenindex temp = new greenindex();
        pinkindex temp2 = new pinkindex();

        for (int i = 1; i < n; i++) {
            //satr 1
            switch (arr[0][i]) {
                case 1:
                    finaly[0][i] = temp.func1(finaly[0][i - 1]);
                    break;
                case 2:
                    finaly[0][i] = temp.func2(finaly[0][i - 1]);
                    break;
                case 3:
                    finaly[0][i] = temp.func3(finaly[0][i - 1]);
                    break;
                case 4:
                    finaly[0][i] = temp.func4(finaly[0][i - 1]);
                    break;
                case 5:
                    finaly[0][i] = temp.func5(finaly[0][i - 1]);
                    break;
            }
            //soton 1
            switch (arr[i][0]) {
                case 1:
                    finaly[i][0] = temp.func1(finaly[i - 1][0]);
                    break;
                case 2:
                    finaly[i][0] = temp.func2(finaly[i - 1][0]);
                    break;
                case 3:
                    finaly[i][0] = temp.func3(finaly[i - 1][0]);
                    break;
                case 4:
                    finaly[i][0] = temp.func4(finaly[i - 1][0]);
                    break;
                case 5:
                    finaly[i][0] = temp.func5(finaly[i - 1][0]);
                    break;
            }
        }

        for (int i = 1; i < n - 1; i++) {
            for (int j = 1; j < n - 1; j++) {
                switch (arr[i][j]) {
                    case 1:
                        finaly[i][j] = temp.func1(finaly[i][j - 1]);
                        break;
                    case 2:
                        finaly[i][j] = temp.func2(finaly[i][j - 1]);
                        break;
                    case 3:
                        finaly[i][j] = temp.func3(finaly[i][j - 1]);
                        break;
                    case 4:
                        finaly[i][j] = temp.func4(finaly[i][j - 1]);
                        break;
                    case 5:
                        finaly[i][j] = temp.func5(finaly[i][j - 1]);
                        break;
                }
            }
            switch (arr[i][n - 1]) {
                case 1:
                    finaly[i][n - 1] = temp2.func1(finaly[i][n - 2], finaly[i - 1][n - 1]);
                    break;
                case 2:
                    finaly[i][n - 1] = temp2.func2(finaly[i][n - 2], finaly[i - 1][n - 1]);
                    break;
                case 3:
                    finaly[i][n - 1] = temp2.func3(finaly[i][n - 2], finaly[i - 1][n - 1]);
                    break;
                case 4:
                    finaly[i][n - 1] = temp2.func4(finaly[i][n - 2], finaly[i - 1][n - 1]);
                    break;
                case 5:
                    finaly[i][n - 1] = temp2.func5(finaly[i][n - 2], finaly[i - 1][n - 1]);
                    break;
            }
            for (int j = 1; j < n - 1; j++) {
                switch (arr[j][i]) {
                    case 1:
                        finaly[j][i] = temp.func1(finaly[j - 1][i]);
                        break;
                    case 2:
                        finaly[j][i] = temp.func2(finaly[j - 1][i]);
                        break;
                    case 3:
                        finaly[j][i] = temp.func3(finaly[j - 1][i]);
                        break;
                    case 4:
                        finaly[j][i] = temp.func4(finaly[j - 1][i]);
                        break;
                    case 5:
                        finaly[j][i] = temp.func5(finaly[j - 1][i]);
                        break;
                }
            }
            switch (arr[n - 1][i]) {
                case 1:
                    finaly[n - 1][i] = temp2.func1(finaly[n - 1][i - 1], finaly[n - 2][i]);
                    break;
                case 2:
                    finaly[n - 1][i] = temp2.func2(finaly[n - 1][i - 1], finaly[n - 2][i]);
                    break;
                case 3:
                    finaly[n - 1][i] = temp2.func3(finaly[n - 1][i - 1], finaly[n - 2][i]);
                    break;
                case 4:
                    finaly[n - 1][i] = temp2.func4(finaly[n - 1][i - 1], finaly[n - 2][i]);
                    break;
                case 5:
                    finaly[n - 1][i] = temp2.func5(finaly[n - 1][i - 1], finaly[n - 2][i]);
                    break;
            }
        }
        switch (arr[n-1][n-1]) {
            case 1:
                finaly[n-1][n-1] = temp2.func1(finaly[n-1][n-2], finaly[n-2][n-1]);
                break;
            case 2:
                finaly[n-1][n-1] = temp2.func2(finaly[n-1][n-2], finaly[n-2][n-1]);
                break;
            case 3:
                finaly[n-1][n-1] = temp2.func3(finaly[n-1][n-2], finaly[n-2][n-1]);
                break;
            case 4:
                finaly[n-1][n-1] = temp2.func4(finaly[n-1][n-2], finaly[n-2][n-1]);
                break;
            case 5:
                finaly[n-1][n-1] = temp2.func5(finaly[n-1][n-2], finaly[n-2][n-1]);
                break;
        }
        return finaly[n-1][n-1] ;
    }
}
